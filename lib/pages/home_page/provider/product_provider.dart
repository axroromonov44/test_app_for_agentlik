import 'package:flutter/material.dart';
import 'package:test_app_for_agentlik/pages/home_page/data/productModel.dart';
import 'package:test_app_for_agentlik/pages/home_page/provider/product_repo.dart';

class ProductProviderController extends ChangeNotifier {
  bool isLoading = false;
  ProductRepo productRepo = ProductRepo();
  ScrollController scroller = ScrollController();
  List<Product> productModel = [];
  int total = 100;
  int limit = 10;

  ProductProviderController() {
    scroller.addListener(() {
      if (scroller.position.pixels == scroller.position.maxScrollExtent &&
          total >= limit &&
          isLoading == false) {
        getMoreProducts();
      }
    });
  }

  Future<void> getProducts() async {
    loading(true);
    Map<String, dynamic> json = await productRepo.getProducts(total, limit);
    total = json['total'];
    List<Product> newProductModel = [];
    newProductModel =
        List<Product>.from(json['products']?.map((i) => Product.fromJson(i)));
    productModel.addAll(newProductModel);
    limit += 5;
    notifyListeners();
    loading(false);
  }

  Future<void> getMoreProducts() async {
    Map<String, dynamic> json = await productRepo.getProducts(total, limit);
    total = json['total'];
    List<Product> newProductModel = [];
    newProductModel =
    List<Product>.from(json['products']?.map((i) => Product.fromJson(i)));
    productModel.addAll(newProductModel);
    limit += 5;
    notifyListeners();
  }

  void loading(bool value) {
    isLoading = value;
    notifyListeners();
  }
}

ProductProviderController providerControllerX = ProductProviderController();
