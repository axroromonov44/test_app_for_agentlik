import 'dart:convert';

import 'package:dio/dio.dart';

class ProductRepo {
  Dio dio = Dio();

  Future<Map<String, dynamic>> getProducts(int total , int limit ) async {
    Response<String> response = await dio.get('https://dummyjson.com/products?total=$total&limit=$limit');
    Map<String, dynamic> json = jsonDecode(response.data!);
    return json;
  }
}
