import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:test_app_for_agentlik/core/state_manager.dart';
import 'package:test_app_for_agentlik/pages/home_page/presentation/widgets/drawer.dart';
import 'package:test_app_for_agentlik/pages/home_page/provider/product_provider.dart';

import '../../../core/notification.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const Drawer(
        child: DrawerLeft(),
      ),
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(254, 76, 76, 1),
        title: const Text('Products'),
      ),
      body: StateManager(
        state: providerControllerX,
        builder: () {
          return providerControllerX.isLoading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : loaded();
        },
      ),
    );
  }

  loaded() {
    return SingleChildScrollView(
      child: Container(
        padding:
            const EdgeInsets.only(left: 12, top: 12, right: 12, bottom: 50),
        height: MediaQuery.of(context).size.height * 0.9,
        child: GridView.builder(
            controller: providerControllerX.scroller,
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 200,
              childAspectRatio: 0.8,
              crossAxisSpacing: 20,
              mainAxisSpacing: 20,
            ),
            itemCount: providerControllerX.productModel.length,
            itemBuilder: (BuildContext ctx, index) {
              return Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.circular(15)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: CachedNetworkImage(
                          imageUrl: providerControllerX
                              .productModel[index].thumbnail!,
                          placeholder: (context, url) => Shimmer.fromColors(
                                baseColor: Colors.grey[300]!,
                                highlightColor: Colors.grey[100]!,
                                child: SizedBox(
                                  height: 40,
                                  width: 50,
                                ),
                              ),
                          errorWidget: (context, url, error) =>
                              const Icon(Icons.error)),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child:
                          Text(providerControllerX.productModel[index].title!),
                    ),
                  ],
                ),
              );
            }),
      ),
    );
  }
}
