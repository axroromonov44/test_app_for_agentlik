import 'package:flutter/material.dart';

class DrawerLeft extends StatelessWidget {
  const DrawerLeft({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          Container(
            height: 150,
            decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage('assets/circleavatar.jpg',),fit: BoxFit.fitHeight)
            ),
          ),
          SizedBox(height: 20,),
          Text('Axror Omonov',style: TextStyle(fontWeight: FontWeight.bold),),
          SizedBox(height: 20,),
          Text('axroromonov44@gmail.com',style: TextStyle(fontWeight: FontWeight.normal),),
          Spacer(),
          Text('Log Out',style: TextStyle(color: Colors.red),)
        ],
      ),
    );
  }
}
