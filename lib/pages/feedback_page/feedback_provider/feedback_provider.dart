import 'package:flutter/material.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class FeedbackProvider extends ChangeNotifier {
  TextEditingController nameController = TextEditingController();
  TextEditingController surNameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController textController = TextEditingController();
  bool isLoading = true;

  void sentRequest(context) async {
    loading(true);
    if (nameController.text.isNotEmpty &&
        surNameController.text.isNotEmpty &&
        phoneController.text.isNotEmpty &&
        textController.text.isNotEmpty) {
      showTopSnackBar(
        Overlay.of(context),
        const CustomSnackBar.success(
          message: "Feedback sent successfully",
          backgroundColor: Color.fromRGBO(254, 76, 76, 1),
          icon: Icon(
            Icons.done_all,
            size: 22,
            color: Colors.white,
          ),
          iconPositionLeft: 24,
          iconRotationAngle: 24,
        ),
      );
      nameController.clear();
      surNameController.clear();
      phoneController.clear();
      textController.clear();
      notifyListeners();
      loading(false);
    }
  }

  void loading(bool value) {
    isLoading = value;
    notifyListeners();
  }
}

FeedbackProvider feedbackProviderX = FeedbackProvider();
