import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:test_app_for_agentlik/core/custom_btn.dart';
import 'package:test_app_for_agentlik/core/custom_textfield.dart';
import 'package:test_app_for_agentlik/core/state_manager.dart';
import 'package:test_app_for_agentlik/pages/feedback_page/feedback_provider/feedback_provider.dart';

class FeedbackPage extends StatefulWidget {
  const FeedbackPage({Key? key}) : super(key: key);

  @override
  State<FeedbackPage> createState() => _FeedbackPageState();
}

class _FeedbackPageState extends State<FeedbackPage> {
  @override
  Widget build(BuildContext context) {
    return KeyboardDismisser(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Color.fromRGBO(254, 76, 76, 1),
          title: const Text('Feedback'),
        ),
        body: StateManager(
          state: feedbackProviderX,
          builder: () {
            return Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  CustomTextField(
                    controller: feedbackProviderX.nameController,
                    hintText: 'Name',
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  CustomTextField(
                    controller: feedbackProviderX.surNameController,
                    hintText: 'Surname',
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  CustomTextField(
                    controller: feedbackProviderX.phoneController,
                    hintText: 'Phone number',
                    keyboardType: TextInputType.phone,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all()),
                    child: TextField(
                      controller: feedbackProviderX.textController,
                      maxLines: 5,
                      decoration: const InputDecoration(
                          border: InputBorder.none, hintText: 'Feedback Text ...'),
                    ),
                  ),
                  Spacer(),
                  CustomBtn(
                      onPressed: () {
                        feedbackProviderX.sentRequest(context);
                      },
                      child: const Text(
                        'Send Feedback',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                        ),
                      ))
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
