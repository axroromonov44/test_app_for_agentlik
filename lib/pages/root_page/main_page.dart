import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_app_for_agentlik/pages/home_page/presentation/home_page.dart';
import 'package:test_app_for_agentlik/pages/home_page/provider/product_provider.dart';

import '../../core/notification.dart';
import '../feedback_page/feedback_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int tabIndex = 0;

  @override
  void initState() {
    providerControllerX.getProducts();
    notificationService.showScheduledNotification(
        id: 0,
        title: 'for agentlik',
        body: 'welcome',
        duration: Duration(seconds: 5));
    notificationService.showScheduledNotification(
        id: 1,
        title: 'for agentlik',
        body: 'have a good day',
        duration: Duration(seconds: 15));
    notificationService.showScheduledNotification(
        id: 2,
        title: 'for agentlik',
        body: 'thanks',
        duration: Duration(seconds: 25));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: tabIndex,
        children: [
          HomePage(),
          FeedbackPage()
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          elevation: 0,
          unselectedItemColor: const Color.fromRGBO(79, 79, 79, 1),
          selectedItemColor: const Color.fromRGBO(254, 76, 76, 1),
          selectedFontSize: 12,
          unselectedFontSize: 12,
          onTap: (int index) {
            setState(() {
              tabIndex = index;
            });
          },
          currentIndex: tabIndex,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.home), label: 'Home '),
            BottomNavigationBarItem(
                icon: Icon(CupertinoIcons.chat_bubble), label: 'Feedback'),
          ]),
    );
  }
}
