import 'package:flutter/material.dart';
import 'package:flutter_screen_lock/flutter_screen_lock.dart';
import 'package:hive/hive.dart';
import 'package:test_app_for_agentlik/pages/home_page/provider/product_provider.dart';
import 'package:test_app_for_agentlik/pages/root_page/main_page.dart';

class PinCodePage extends StatefulWidget {
  const PinCodePage({Key? key}) : super(key: key);

  @override
  State<PinCodePage> createState() => _PinCodePageState();
}

class _PinCodePageState extends State<PinCodePage> {
  final _contactBox = Hive.box('isSignIn');
  @override
  void initState() {
    providerControllerX.getProducts();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: 900,
              child: _contactBox.get('isLock') != null
                  ? ScreenLock(
                      correctString: _contactBox.get('isLock'),
                      onCancelled: Navigator.of(context).pop,
                      onUnlocked: () {
                        Navigator.push(context, MaterialPageRoute(builder: (_) => MainPage()));
                      },
                    )
                  : ScreenLock.create(
                      onCancelled: Navigator.of(context).pop,
                      onConfirmed: (String value) {
                        _contactBox.put('isLock',value);
                        Navigator.push(context, MaterialPageRoute(builder: (_) => MainPage()));
                      },
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
