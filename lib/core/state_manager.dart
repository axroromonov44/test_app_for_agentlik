import 'package:flutter/material.dart';

class StateManager extends StatefulWidget {
  final ChangeNotifier state;
  final Widget Function() builder;

  StateManager({
    required this.state,
    required this.builder,
  });

  @override
  createState() => _StateManagerState();
}

class _StateManagerState extends State<StateManager> {
  void rebuilder() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    widget.state.addListener(rebuilder);
  }

  @override
  void dispose() {
    widget.state.removeListener(rebuilder);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.builder();
  }
}
