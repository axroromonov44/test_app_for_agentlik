
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/rxdart.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest.dart' as tz;

class LocalNotificationService {
  final FlutterLocalNotificationsPlugin _notificationsPlugin = FlutterLocalNotificationsPlugin();
  final BehaviorSubject<String?> onNotificationClick = BehaviorSubject();

  //NOTIFICATION SETTINGS
  final InitializationSettings notificationSettings = InitializationSettings(
    android: AndroidInitializationSettings('@mipmap/ic_launcher'),
    iOS: IOSInitializationSettings(
      requestAlertPermission: true,
      requestBadgePermission: true,
      requestSoundPermission: true,
      onDidReceiveLocalNotification: (id, title, body, payload) {},
    ),
  );

  //NOTIFICATION DETAILS
  final NotificationDetails notificationDetails = NotificationDetails(
    android: AndroidNotificationDetails(
      'channel_id',
      'channel_name',
      importance: Importance.max,
      priority: Priority.max,
      // color: Colors.white,
      icon: '@mipmap/ic_launcher',
      channelShowBadge: false,
      largeIcon: DrawableResourceAndroidBitmap('@drawable/ic_launcher'),
    ),
    iOS: IOSNotificationDetails(),
  );

  LocalNotificationService() {
    initialize();
  }

  Future<void> initialize() async {
    //INITIALIZE TIME ZONE PACKAGE
    tz.initializeTimeZones();

    //INITIALIZE LOCAL NOTIFICATION
    await _notificationsPlugin.initialize(notificationSettings, onSelectNotification: (payload) {
      if (payload != null && payload.isNotEmpty) {
        onNotificationClick.add(payload);
      }
    });
  }

  //SHOW NOTIFICATION METHODS

  Future<void> showNotification({required int id, required String title, required String body, String? payload}) async {
    await _notificationsPlugin.show(id, title, body, notificationDetails, payload: payload);
  }

  Future<void> showScheduledNotification({required int id, required String title, required String body, required Duration duration, String? payload}) async {
    await _notificationsPlugin.zonedSchedule(
      id,
      title,
      body,
      tz.TZDateTime.from(DateTime.now().add(duration), tz.local),
      notificationDetails,
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime,
      payload: payload,
    );
  }
}

LocalNotificationService notificationService = LocalNotificationService();
