import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  TextEditingController? controller;
  String? hintText;
  TextInputType? keyboardType;
  void Function(String)? onChanged;
  void Function()? onEditingComplete;
  Widget? prefix;
  TextCapitalization? textCapitalization;

  CustomTextField(
      {Key? key,
        this.controller,
        this.hintText,
        this.onEditingComplete,
        this.prefix,
        this.keyboardType,
        this.onChanged,
        this.textCapitalization,})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15), border: Border.all()),
      child: TextField(
        onChanged: onChanged,
        onEditingComplete: onEditingComplete,
        keyboardType: keyboardType,
        controller: controller,
        textCapitalization: textCapitalization ?? TextCapitalization.none,
        decoration: InputDecoration(
            prefixIcon: prefix, border: InputBorder.none, hintText: hintText),
      ),
    );
  }
}
